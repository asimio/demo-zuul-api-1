# README #

Zuul demo API 1's accompanying source code for blog entry at http://tech.asimio.net/2017/10/10/Routing-requests-and-dynamically-refreshing-routes-using-Spring-Cloud-Zuul-Server.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Eureka server instance - [https://bitbucket.org/asimio/discoveryserver/](https://bitbucket.org/asimio/discoveryserver/)
* Zuul edge server instance - [https://bitbucket.org/asimio/zuulserver/](https://bitbucket.org/asimio/zuulserver/)

### Building and executing the application from command line ###

```
mvn clean package
java -DhostName=$HOSTNAME -Deureka.client.serviceUrl.defaultZone="http://$HOSTNAME:8001/eureka/,http://$HOSTNAME:8002/eureka/" -jar target/demo-zuul-api-1.jar
```

### Available endpoints

`http://localhost:8600/actors/{id}`

### Who do I talk to? ###

* ootero at asimio dot net
* [https://www.linkedin.com/in/ootero](https://www.linkedin.com/in/ootero)